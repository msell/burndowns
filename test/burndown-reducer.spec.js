import burndown from '../src/reducers/burndown';
import firebaseState from './json/firebase-state.json';
import { sprintChanged } from '../src/actions/firebaseActions';
import { initialBurndownState } from '../src/reducers/defaultStates';

describe('burndown reducer', () => {
  it('should convert firebase schema to app state schema', () => {
    const state = burndown(
      initialBurndownState,
       sprintChanged(57, 446, firebaseState)
     );

    expect(state).to.deep.equal(expectedState);
  });
})

var expectedState = {
  title: {
    text: "Burndown Chart",
    x: -20 //center
  },
  colors: ["blue", "red"],
  plotOptions: {
    line: {
      lineWidth: 3
    },
    tooltip: {
      hideDelay: 200
    }
  },
  subtitle: {
    text: "2/8/2017 - 2/20/2017",
    x: -20
  },
  xAxis: {
    categories: [
      "Inital Commitment",
      "Day 1",
      "Day 2",
      "Day 3",
      "Day 4",
      "Day 5",
      "Day 6",
      "Day 7",
      "Day 8",
      "Day 9"
    ]
  },
  yAxis: {
    title: {
      text: "Story Points"
    },
    plotLines: [
      {
        value: 0,
        width: 1
      }
    ]
  },
  tooltip: {
    valueSuffix: " points",
    crosshairs: true,
    shared: true
  },
  legend: {
    layout: "vertical",
    align: "right",
    verticalAlign: "middle",
    borderWidth: 0
  },
  series: [
    {
      name: "Ideal Burn",
      color: "rgba(255,0,0,0.25)",
      lineWidth: 2,
      data: [44, 39.1, 34.2, 29.3, 24.4, 19.6, 14.7, 9.8, 4.9, 0]
    },
    {
      name: "Actual Burn",
      color: "rgba(0,120,200,0.75)",
      marker: {
        radius: 6
      },
      data: [44, 44, 45, 43, 32]
    }
  ]
};
