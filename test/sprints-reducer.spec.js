import sprints from '../src/reducers/sprints';
import sprintData from './json/firebase-sprint.json';
import { sprintDataReceived } from '../src/actions/firebaseActions';

describe('sprint reducer', () => {
  it('should convert firebase schema to app state schema', () => {
    const state = sprints({},
       sprintDataReceived(sprintData)
     );

    expect(state).to.deep.equal(sprintData);
  });
})
