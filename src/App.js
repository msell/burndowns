import React, { Component, PropTypes } from 'react';
import './App.css';
import 'react-select/dist/react-select.css';
import BurndownContainer from './containers/BurndownContainer';
import HeaderContainer from './containers/HeaderContainer';
import { connect } from 'react-redux';
import { fetchTeams } from './actions/firebaseActions';
import {
  BrowserRouter as Router,
  Route,
  Switch
} from 'react-router-dom';

class App extends Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired
  };

  componentWillMount() {
      fetchTeams(this.props.dispatch);
  }

  render() {
    return (
      <div className="App">
        <HeaderContainer />
        <Router>
          <Switch>            
            <Route path="/board/:boardId/sprint/:sprintId" component={BurndownContainer} />
            <Route path="/board/:boardId" component={BurndownContainer} />
            <Route path="/" component={BurndownContainer} />
          </Switch>
        </Router>
      </div>
    );
  }
}

export default connect()(App);
