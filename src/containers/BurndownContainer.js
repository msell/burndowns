import React, { PropTypes } from "react";
import Select from "react-select";
import Burndown from "../components/Burndown";
import { connect } from "react-redux";
import {
  subscribeToSprintData,
  sprintChanged,
  selectTeam,
  selectSprint
} from '../actions/firebaseActions';

class BurndownContainer extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired,
    match: PropTypes.object
  };

  selectTeamHandler = teamId => {
    selectTeam(teamId.value, this.props.teams[teamId.value])(this.props.dispatch);
  };

  selectSprintHandler = sprintId => {
    this.props.dispatch(selectSprint(sprintId.value,
      this.props.sprints[sprintId.value].sprintName));
  }

  componentWillMount(nextProps) {
    const {selectedBoard, selectedSprint} = this.props.selections;
    const {boardId, sprintId} = this.props.match.params; // from router

    subscribeToSprintData(boardId || selectedBoard.value, sprintId || selectedSprint.value)(this.props.dispatch);
    this.props.dispatch(sprintChanged(
      boardId || selectedBoard.value,
      sprintId || selectedSprint.value,
      this.props.sprints[sprintId || selectedSprint.value]));

      console.log({boardId, sprintId});
      if(boardId) {
        selectTeam(boardId, this.props.teams[boardId])(this.props.dispatch);
      };
      // if(sprintId){
      //   this.props.dispatch(selectSprint(sprintId,
      //     this.props.sprints[sprintId].sprintName));
      // }
      // this.selectTeamHandler(this.teamDropdownData[boardId] || selectedBoard)
      // this.selectSprintHandler(this.sprintDropdownData[sprintId] || selectedSprint);

  };

  sprintDropdownData = () => {
    return Object.keys(this.props.sprints).map(x => {
      return {
        value: x, label: this.props.sprints[x]
      }
    })
  };

  teamDropdownData = () => {
    return Object.keys(this.props.teams).map(x => {
      return {
        value: x, label: this.props.teams[x]
      }
    })
  };

  render() {

    return (
      <div>
        <Burndown config={this.props.burndown} />
        <br />

        <Select
          value={this.props.selections.selectedBoard || null}
          options={this.teamDropdownData()}
          onChange={this.selectTeamHandler}
        />
        <Select
          value={this.props.selections.selectedSprint || null}
          options={this.sprintDropdownData()}
          onChange={this.selectSprintHandler}
        />
      </div>
    );
  }
}

// function mapDispatchToProps(dispatch) {
//   subscribeToDatabase(dispatch);
//   return {};
// };

function mapStateToProps({ burndown, sprints, selections, teams }) {
  return { burndown, sprints, selections, teams };
}

export default connect(mapStateToProps)(BurndownContainer);
