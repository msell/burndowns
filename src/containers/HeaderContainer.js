import React from 'react';
import { connect } from 'react-redux';
import logo from '../graph.svg';

class HeaderContainer extends React.Component {
  constructor() {
    super();
    this.sprints = [];
  }

  render() {

    return (
      <div className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <h2>Team { this.props.teams[this.props.selectedBoard.value] }</h2>
      </div>
    )
  }
}

function mapStateToProps({selections, teams}) {
  const { selectedBoard } = selections;
  return {selectedBoard, teams};
};

export default connect(mapStateToProps)(HeaderContainer);
