import { SELECTED_TEAM_CHANGED, SELECTED_SPRINT_CHANGED } from '../actions/actionTypes';

const defaultState =  {
    selectedBoard: {
      value: '57',
      label: 'Phoenix'
    },
    selectedSprint: {
      value: '446',
      label: 'ECID Sprint 60'
    }
  };

export const selections = (state = defaultState, action) => {
  switch (action.type) {
    case SELECTED_TEAM_CHANGED:
      return Object.assign({}, state, {
        selectedBoard: {
          value: action.teamId,
          label: action.name
      }});
    case SELECTED_SPRINT_CHANGED:
      return Object.assign({}, state, {
        selectedSprint: {
          value: action.sprintId,
          label: action.name
        }
      });
    default:
      return state;
  }
}

export default selections;
