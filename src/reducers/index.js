import burndown from './burndown';
import sprints from './sprints';
import teams from './teams';
import selections from './selections';
import { combineReducers } from 'redux';

const reducers = combineReducers({
  burndown,
  sprints,
  selections,
  teams
}
);

export default reducers;
