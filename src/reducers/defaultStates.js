export const mockBurndown = {
  title: {
    text: "Burndown Chart",
    x: -20 //center
  },
  colors: ["blue", "red"],
  plotOptions: {
    line: {
      lineWidth: 3
    },
    tooltip: {
      hideDelay: 200
    }
  },
  subtitle: {
    text: "Sprint 1",
    x: -20
  },
  xAxis: {
    categories: [
      "Inital Commitment",
      "Day 1",
      "Day 2",
      "Day 3",
      "Day 4",
      "Day 5",
      "Day 6",
      "Day 7",
      "Day 8",
      "Day 9"
    ]
  },
  yAxis: {
    title: {
      text: "Story Points"
    },
    plotLines: [
      {
        value: 0,
        width: 1
      }
    ]
  },
  tooltip: {
    valueSuffix: " points",
    crosshairs: true,
    shared: true
  },
  legend: {
    layout: "vertical",
    align: "right",
    verticalAlign: "middle",
    borderWidth: 0
  },
  series: [
    {
      name: "Ideal Burn",
      color: "rgba(255,0,0,0.25)",
      lineWidth: 2,
      data: [44, 39.1, 34.2, 29.3, 24.4, 19.5, 14.6, 9.7, 4.8, 0]
    },
    {
      name: "Actual Burn",
      color: "rgba(0,120,200,0.75)",
      marker: {
        radius: 6
      },
      data: [44, 45, 40, 40, 35, 33, 32, 21, 14, 5]
    }
  ]
};

export const initialBurndownState = {
  title: {
    text: "Burndown Chart",
    x: -20 //center
  },
  colors: ["blue", "red"],
  plotOptions: {
    line: {
      lineWidth: 3
    },
    tooltip: {
      hideDelay: 200
    }
  },
  subtitle: {
    text: "Sprint 1",
    x: -20
  },
  xAxis: {
    categories: [
      "Inital Commitment",
      "Day 1",
      "Day 2",
      "Day 3",
      "Day 4",
      "Day 5",
      "Day 6",
      "Day 7",
      "Day 8",
      "Day 9"
    ]
  },
  yAxis: {
    title: {
      text: "Story Points"
    },
    plotLines: [
      {
        value: 0,
        width: 1
      }
    ]
  },
  tooltip: {
    valueSuffix: " points",
    crosshairs: true,
    shared: true
  },
  legend: {
    layout: "vertical",
    align: "right",
    verticalAlign: "middle",
    borderWidth: 0
  },
  series: [
    {
      name: "Ideal Burn",
      color: "rgba(255,0,0,0.25)",
      lineWidth: 2,
      data: []
    },
    {
      name: "Actual Burn",
      color: "rgba(0,120,200,0.75)",
      marker: {
        radius: 6
      },
      data: []
    }
  ]
};
