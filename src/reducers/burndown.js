import { initialBurndownState } from './defaultStates';
import { SPRINT_CHANGED } from '../actions/actionTypes';

export const burndown = (state = initialBurndownState, action) => {
  switch(action.type){
  case SPRINT_CHANGED:
    if(!action.data){
      return state;
    }
    // console.log(JSON.stringify(action.data, null, 2));
    const data = action.data[action.sprintId];
    let actualBurnPoints =  data.dataPoints.filter(x => {
      return x !== null;
    });
    const initialCommitment = actualBurnPoints[0];
    actualBurnPoints = [initialCommitment, ...actualBurnPoints];
    const idealBurnRate = initialCommitment / data.daysInSprint;
    let burndownCount = 0;
    let idealBurnPoints = [0];
    for(let i=0; i<data.daysInSprint; i++) {
      burndownCount += idealBurnRate;
      idealBurnPoints.push(Number(burndownCount.toFixed(1)));
    };
    idealBurnPoints = idealBurnPoints.reverse();
    // console.log({
    //   initialCommitment,
    //   idealBurnRate,
    //   idealBurnPoints,
    //   actualBurnPoints
    // });

    const mergeObject = {
      subtitle: {
        text: `${data.startDate} - ${data.endDate}`,
        x: -20
      },
      series: [
        {
          name: "Ideal Burn",
          color: "rgba(255,0,0,0.25)",
          lineWidth: 2,
          data: idealBurnPoints
        },
        {
          name: "Actual Burn",
          color: "rgba(0,120,200,0.75)",
          marker: {
            radius: 6
          },
          data: actualBurnPoints
        }
      ]
    }
// /    const serverData = action.data.burndowns;
    // TODO: If we want to just limit our state to a single burndown which is the plan
    // then we also need to know which teamId and sprintId is currently selected.
    return Object.assign({}, state, mergeObject)
  default:
    return state;
  }
}

export default burndown;
