import { SPRINT_DATA_RECEIVED } from '../actions/actionTypes';

export const sprintBoards = (state = {}, action) => {
  switch(action.type){
  case SPRINT_DATA_RECEIVED:
    if(!action.data) {
      return state;
    };

    return action.data;
  default:
    return state;
  }
}

export default sprintBoards;
