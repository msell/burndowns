import { TEAMS_RECEIVED } from '../actions/actionTypes';

export const teams = (state = {}, action) => {
  switch(action.type){
    case TEAMS_RECEIVED:
      const { data } = action;
      return data;
    default:
      return state;
  }
}

export default teams;
