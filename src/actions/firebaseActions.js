import {
  SPRINT_CHANGED,
  DATA_CHANGED,
  TEAMS_RECEIVED,
  SPRINT_DATA_RECEIVED,
  SELECTED_TEAM_CHANGED,
  SELECTED_SPRINT_CHANGED
} from './actionTypes';
import database from '../firebase/database';

export const dataChanged = data => {
  return {
    type: DATA_CHANGED,
    data
  }
};

export const selectSprint = (sprintId, name) => {
  return {
    type: SELECTED_SPRINT_CHANGED,
    sprintId,
    name
  }
};

export const sprintChanged = (teamId, sprintId, data) => {
  return {
    type: SPRINT_CHANGED,
    teamId,
    sprintId,
    data
  }
};

export const sprintDataReceived = data => {
  return {
    type: SPRINT_DATA_RECEIVED,
    data
  }
};

export const selectTeam = (teamId, name) => dispatch => {

  dispatch( {
    type: SELECTED_TEAM_CHANGED,
    teamId,
    name
  });

  database.ref(`burndowns/${teamId}`).on('value', snapshot => {
    const data = snapshot.val();
    dispatch(sprintDataReceived(Object.assign(data)));
    const sprintIds = Object.keys(data);
    if(Array.isArray(sprintIds) && sprintIds.length > 0) {
      const latestSprint = sprintIds[sprintIds.length -1];
      dispatch(selectSprint(latestSprint, data[latestSprint].sprintName));
      dispatch(sprintChanged(teamId, latestSprint, data));
    }

});
};

export const subscribeToDatabase = dispatch => {
  database.ref().on('value', snapshot => {
    dispatch(dataChanged(snapshot.val()));
  });
};

export const subscribeToSprintData = (teamId, sprintId) => dispatch => {
  database.ref(`burndowns/${teamId}`).on('value', snapshot => {
    const data = snapshot.val();
    dispatch(sprintDataReceived(Object.assign(data)));
    dispatch(sprintChanged(teamId, sprintId, data))
  });
};

export const fetchTeams = dispatch => {
  database.ref('teams').once('value').then(snapshot => {
    dispatch({
      type: TEAMS_RECEIVED,
      data: snapshot.val()
    });
  });
};
